APP=ingress-nginx
./kubetools kubectl create ns ${APP}

./kubetools helm3 upgrade -i -n ${APP} ${APP} https://github.com/kubernetes/ingress-nginx/releases/download/helm-chart-4.0.12/ingress-nginx-4.0.12.tgz -f ./ingress/values.yaml

