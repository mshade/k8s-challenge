curl https://raw.githubusercontent.com/argoproj/argo-cd/master/manifests/install.yaml | \
  sed -e 's/namespace: argocd/namespace: default/' | \
  ./kubetools kubectl apply -f -

./kubetools kubectl apply -f argo/ingress.yaml
./kubetools kubectl apply -f argo/argo-apps.yaml

