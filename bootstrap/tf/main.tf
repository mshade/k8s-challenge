terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "2.16.0"
    }
  }
}

variable "do_token" {
  type        = string
  description = "Digitalocean API Token"
}

provider "digitalocean" {
  token = var.do_token
}

resource "digitalocean_kubernetes_cluster" "dok8s" {
  name    = "dok8s"
  region  = "nyc3"
  version = "1.21.5-do.0"

  node_pool {
    name       = "workers-2x4"
    size       = "s-2vcpu-4gb"
    node_count = 1
  }
}

variable "kubeconfig_path" {
  description = "The path to save the kubeconfig to"
}

resource "local_file" "kubeconfig" {
  content         = digitalocean_kubernetes_cluster.dok8s.kube_config[0].raw_config
  filename        = pathexpand(var.kubeconfig_path)
  file_permission = "0600"
}

